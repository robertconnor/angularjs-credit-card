const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

/**
 * Env
 * Get npm lifecycle event to identify the environment
 */
const debug = process.env.npm_lifecycle_event !== 'build';

if (debug) {
    console.log('Running webpack for development!');
} else {
    console.log('Running webpack for production!');
}

module.exports = function initWebpackConfig() {
    // Init config object
    var config = {};

    /**
     * Entry point
     */
    config.entry = {
        angular_credit_card: './src/index.js'
    };

    /**
     * Output path and filename
     */
    config.output = {
        // Absolute output directory
        path: path.resolve(__dirname, 'dist/js'),

        // Filename for entry points
        // Only adds hash in build mode
        filename: '[name].min.js'
    };

    /**
     * Devtool
     * Reference: http://webpack.github.io/docs/configuration.html#devtool
     * Type of sourcemap to use per build type
     */
    config.devtool = (debug) ? 'inline-source-map' : '';

    /**
   * Loaders
   * Reference: http://webpack.github.io/docs/configuration.html#module-loaders
   * List: http://webpack.github.io/docs/list-of-loaders.html
   * This handles most of the magic responsible for converting modules
   */
    config.module = {
        rules: [
            {
                // JS LOADER
                // Reference: https://github.com/babel/babel-loader
                // Transpile .js files using babel-loader
                // Compiles ES6 and ES7 into ES5 code
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /(node_modules|bower_components)/,
                query: {
                    //presets: ['react', 'es2015', 'stage-0'],
                    //plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
                }
            },

            {
                // CSS LOADER
                // Reference: https://github.com/webpack/css-loader
                // Allow loading css through js
                //
                // Reference: https://github.com/postcss/postcss-loader
                // Postprocess your css with PostCSS plugins
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        { loader: 'css-loader', options: { sourceMap: true, minimize: (debug) ? false : true } },
                        { loader: 'postcss-loader' }
                    ],
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        { loader: 'css-loader', options: { sourceMap: true, minimize: (debug) ? false : true } },
                        { loader: 'postcss-loader', options: { sourceMap: true, minimize: (debug) ? false : true } },
                        { loader: 'sass-loader', options: { sourceMap: true } }
                    ]
                })
            }
        ]
    };

    /**
     * Plugins
     * Reference: http://webpack.github.io/docs/configuration.html#plugins
     * List: http://webpack.github.io/docs/list-of-plugins.html
     */
    config.plugins = [
        new CleanWebpackPlugin([
            'dist',
            'index.html'
        ]),
        new ExtractTextPlugin('../css/[name].min.css'),
        new CopyWebpackPlugin([
            { from: 'src/images', to: '../images' },
            { from: 'src/views', to: '../../' }
        ])
    ];

    if (!debug) {
        config.plugins.push(
            new webpack.optimize.OccurrenceOrderPlugin(),
            new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false })
        );
    }

    return config;
}
