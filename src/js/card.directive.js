'use strict';

angular
  .module('TheBug')
  .directive('card', ['$timeout', function($timeout) {
    return {
        restrict  : 'A',
        scope     : {
            cardContainer: '@',
            width        : '@',
            placeholders : '=',
            options      : '=',
            messages     : '='
        },
        controller: 'ReservedCreditCardController',
        link      : function(scope, element, attributes, cardCtrl) {
            var defaultPlaceholders = {
                name  : 'Your Name',
                number: 'xxxx xxxx xxxx xxxx',
                expiry: 'MM/YY',
                cvc   : 'xxx',
                type  : ''
            };

            var defaultMessages = {
                validDate: 'valid\nthru',
                monthYear: 'month/year',
            };

            var defaultOptions = {
                debug     : false,
                formatting: true
            };

            var placeholders = angular.extend(defaultPlaceholders, scope.placeholders);
            var messages     = angular.extend(defaultMessages, scope.messages);
            var options      = angular.extend(defaultOptions, scope.options);

            var cardOptions = {
                form         : '[name="' + attributes.name + '"]',
                container    : scope.cardContainer,
                formSelectors: {},
                width        : options.width,
                messages     : {
                    validDate: messages.validDate,
                    monthYear: messages.monthYear
                },
                placeholders : {
                    number: placeholders.number,
                    name  : placeholders.name,
                    expiry: placeholders.expiry,
                    cvc   : placeholders.cvc,
                    type  : placeholders.type
                },
                formatting   : options.formatting,
                debug        : options.debug
            };

            cardOptions.width = cardOptions.width || scope.width || 350;

            if (cardCtrl.numberInput && cardCtrl.numberInput.length > 0) {
                cardOptions.formSelectors.numberInput = 'input[name="' + cardCtrl.numberInput[0].name + '"]';
            }

            if (angular.isDefined(cardCtrl.expiryInput.combined)) {
                cardOptions.formSelectors.expiryInput = 'input[name="' + cardCtrl.expiryInput.combined[0].name + '"]';
            } else if (angular.isDefined(cardCtrl.expiryInput.month) && angular.isDefined(
                cardCtrl.expiryInput.year)) {
                cardOptions.formSelectors.expiryInput = 'input[name="' + cardCtrl.expiryInput.month[0].name + '"], ' +
                    'input[name="' + cardCtrl.expiryInput.year[0].name + '"]';
            }

            if (cardCtrl.cvcInput && cardCtrl.cvcInput.length > 0) {
                cardOptions.formSelectors.cvcInput = 'input[name="' + cardCtrl.cvcInput[0].name + '"]';
            }

            if (cardCtrl.nameInput && cardCtrl.nameInput.length > 0) {
                cardOptions.formSelectors.nameInput = 'input[name="' + cardCtrl.nameInput[0].name + '"]';
            }

            if (cardCtrl.cardTypeInput && cardCtrl.cardTypeInput.length > 0) {
                cardOptions.formSelectors.cardTypeInput = 'input[name="' + cardCtrl.cardTypeInput[0].name + '"]';
            }

            // Initialize card after angular has updated the DOM with any interpolated bindings
            $timeout(angular.noop)
                .then(function() {
                    scope.$parent.cardObject = new Card(cardOptions);
                });
        }
    };
  }]);
