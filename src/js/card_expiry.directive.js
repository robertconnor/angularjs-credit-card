'use strict';

angular
  .module('TheBug')
  .directive('cardExpiry', ['$compile', function($compile) {
    return {
      restrict: 'A',
      scope   : {
        ngModel: '=',
        type   : '@cardExpiry'
      },
      require: [
        '^card',
        'ngModel'
      ],
      link: function(scope, element, attributes, ctrls) {
        var cardCtrl = ctrls[0];
        var expiryType = scope.type || 'combined';

        if (angular.isUndefined(cardCtrl.expiryInput)) {
          cardCtrl.expiryInput = {};
        }

        cardCtrl.expiryInput[expiryType] = element;
        scope.$watch('ngModel', function(newVal, oldVal) {
          if (!oldVal && !newVal) {
            return;
          }

          if (oldVal === newVal && !newVal) {
            return;
          }

          var evt = document.createEvent('HTMLEvents');
          evt.initEvent('keyup', false, true);
          element[0].dispatchEvent(evt);
        });
      }
    };
  }]);
