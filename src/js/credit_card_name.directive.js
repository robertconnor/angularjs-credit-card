'use strict';

angular
  .module('TheBug')
  .directive('creditCardName', ['$compile', function($compile) {
    return {
      restrict: 'A',
      scope   : {
        ngModel      : '=',
        cardNameModel: '=',
        cardTypeModel: '='
      },
      require: [
        '^card',
        'ngModel'
      ],
      link: function(scope, element, attributes, ctrls) {
        scope.$watch('cardTypeModel', function(value) {
          scope.ngModel = value;
        });

        scope.$watch('cardNameModel', function(value) {
          if (value) {
            scope.ngModel = value + '\'s ' + scope.cardTypeModel;
          } else {
            scope.ngModel = scope.cardTypeModel;
          }
        });
      }
    };
  }]);
  