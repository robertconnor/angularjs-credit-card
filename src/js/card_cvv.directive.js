'use strict';

angular
  .module('TheBug')
  .directive('cardCvc', ['$compile', function($compile) {
    return {
      restrict: 'A',
      scope   : {
        ngModel: '='
      },
      require: [
        '^card',
        'ngModel'
      ],
      link: function(scope, element, attributes, ctrls) {
        var cardCtrl = ctrls[0];
        cardCtrl.cvcInput = element;

        scope.$watch('ngModel', function(newVal, oldVal) {
          if (!oldVal && !newVal) {
            return;
          }

          if (oldVal === newVal && !newVal) {
            return;
          }

          var evt = document.createEvent('HTMLEvents');
          evt.initEvent('keyup', false, true);
          element[0].dispatchEvent(evt);
        });
      }
    };

  }]);
