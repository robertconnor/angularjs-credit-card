'use strict';

angular
  .module('TheBug')
  .directive('cardNumber', ['$compile', '$rootScope', function($compile, $rootScope) {
    return {
      restrict: 'A',
      scope: {
        ngModel      : '=',
        cardTypeModel: '='
      },
      require: [
        '^card',
        'ngModel'
      ],
      link: function(scope, element, attributes, ctrls) {
        var cardCtrl = ctrls[0];
        cardCtrl.numberInput = element;

        scope.$watch('ngModel', function(newVal, oldVal) {
          if (!oldVal && !newVal) {
            return;
          }

          if (oldVal === newVal && !newVal) {
            return;
          }

          if (newVal.length >= 3) {
            scope.cardTypeModel = scope.$parent.cardObject.cardType;
          } else {
            scope.cardTypeModel = '';
          }

          var evt = document.createEvent('HTMLEvents');
          evt.initEvent('keyup', false, true);
          element[0].dispatchEvent(evt);
        });
      }
    };
  }]);
